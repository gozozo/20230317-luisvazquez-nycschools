//
//  NSSchoolsEntity.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

public struct NSSchoolsEntity: Codable {
    let dbn: String
    let schoolName: String
    let location: String
    let overviewParagraph: String
    let phoneNumber: String
    let website: String
    let primaryAddressLine1: String
    let city: String
    let zip: String
    let stateCode: String
    let schoolEmail: String?
    let latitude: String?
    let longitude: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case location
        case overviewParagraph = "overview_paragraph"
        case phoneNumber = "phone_number"
        case website
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case schoolEmail = "school_email"
        case latitude
        case longitude
    }
}
