//
//  NSSchoolsInteractor.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation
import Alamofire

final class NSSchoolsInteractor {
    private weak var presenter: NSSchoolsInteractorOutputProtocol?

    func fetchSchools() {
        AF.request("https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
            .validate()
            .responseDecodable(of: [NSSchoolsEntity].self) { response in
                switch response.result {
                case .success(let data):
                    self.presenter?.showSchools(schools: data)
                case .failure(let error):
                    self.presenter?.showError(error.localizedDescription)
                }
            }
       }
}

// MARK: - Extension -

extension NSSchoolsInteractor: NSSchoolsInteractorProtocol {
    func attach(presenter: NSSchoolsInteractorOutputProtocol?) {
        self.presenter = presenter
    }
}
