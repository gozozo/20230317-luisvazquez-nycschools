//
//  NSSchoolsViewController.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class NSSchoolsViewController: UIViewController {

    // MARK: Presenter
    private(set) var presenter: NSSchoolsPresenterProtocol?

    // MARK: Constructor
    init(presenter: NSSchoolsPresenterProtocol) {
        self.presenter = presenter
        super.init( nibName: "NSSchoolsViewController", bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: IBOUtlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: Properties
    var items: [NSSchoolsEntity] = []

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.presenter?.viewDidLoad()
    }

    // MARK: Private methods
    private func setupView() {
        setupTableView()
    }

    private func setupTableView() {
        self.title = "NYC Schools"
        tableView.delegate = self
        tableView.dataSource = self
        // Register the custom cell class with the table view
        tableView.register(UINib(nibName: "NSSchoolTableViewCell", bundle: nil), forCellReuseIdentifier: "customCell")
    }

    internal func showSchools(with schools: [NSSchoolsEntity]) {
        items = schools
        tableView.reloadData()
    }

    // MARK: Public methods
}

// MARK: - Extensions -
extension NSSchoolsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.numberOfRowsInSection(section: section) ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as? NSSchoolTableViewCell else {
            return UITableViewCell()
        }

        guard let school = self.presenter?.cellForRowAt(indexPath: indexPath) else {
            return UITableViewCell()
        }

        cell.configureCell(school: school)
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.presenter?.titleForHeaderInSection(section: section)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.presenter?.heightForRowAt(widthContainer: tableView.frame.width, indexPath: indexPath) ?? 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.self.presenter?.didSelectRowAt(indexPath: indexPath)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter?.numberOfSections() ?? 0
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.presenter?.sectionIndexTitles()
    }
}

extension NSSchoolsViewController: NSSchoolsViewProtocol {

    func showError(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(actionOk)
        present(alertController, animated: true, completion: nil)
    }

    func showSchools() {
        tableView.reloadData()
    }
}
