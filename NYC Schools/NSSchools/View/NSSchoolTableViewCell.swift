//
//  NSSchoolTableViewCell.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//

import Foundation
import UIKit

class NSSchoolTableViewCell: UITableViewCell {

    // Outlets for the UI elements in the cell
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    // Customize the appearance of the cell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Add an arrow to indicate that the cell can be navigated
        accessoryType = .disclosureIndicator
    }

    // Configure the contents of the cell
    func configureCell(school: NSSchoolsEntity) {
        titleLabel.text = school.schoolName
        subtitleLabel.text = "\(school.primaryAddressLine1), \(school.city) \(school.stateCode) \(school.zip)"
    }
}
