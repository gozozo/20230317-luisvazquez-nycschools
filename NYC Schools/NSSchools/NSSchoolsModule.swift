//
//  NSSchoolsModule.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import UIKit

public final class NSSchoolsModule {
    private var presenter: NSSchoolsPresenterProtocol

    public init(baseController: UIViewController?) {
        let router = NSSchoolsRouter(parentController: baseController)
        let interactor = NSSchoolsInteractor()
        self.presenter = NSSchoolsPresenter(
            interactor: interactor,
            router: router
        )
    }

    public func present() {
        self.presenter.present()
    }
}
