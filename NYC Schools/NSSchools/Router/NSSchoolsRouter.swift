//
//  NSSchoolsRouter.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class NSSchoolsRouter {
    private weak var parentController: UIViewController?

    init(parentController: UIViewController?) {
        self.parentController = parentController
    }

}

// MARK: - Extension -

extension NSSchoolsRouter: NSSchoolsRouterProtocol {

    func present(presenter: NSSchoolsPresenterProtocol) {
        let controller = NSSchoolsViewController(presenter: presenter)
        presenter.attach(view: controller)
        if let navigation = parentController?.navigationController {
            navigation.pushViewController(controller, animated: true)
        }
    }

    func goToSchoolDetail(parent: NSSchoolsViewProtocol?, school: NSSchoolsEntity) {
        NSSchoolsDetailModule.init(baseController: parent as? UIViewController, school: school).present()
    }
}
