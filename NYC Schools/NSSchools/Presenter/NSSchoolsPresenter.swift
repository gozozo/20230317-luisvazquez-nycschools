//
//  NSSchoolsPresenter.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import UIKit

final class NSSchoolsPresenter {
    private weak var view: NSSchoolsViewProtocol?
    private var interactor: NSSchoolsInteractorProtocol
    private var router: NSSchoolsRouterProtocol

    private var sections: [String] = []
    private var items: [String: [NSSchoolsEntity]] = [:]

    private let staticHeightCell: CGFloat = 45
    private let staticWidthCell: CGFloat = 62
    private let titleNumberOfLines: Int = 2

    init(
        interactor: NSSchoolsInteractorProtocol,
        router: NSSchoolsRouterProtocol
    ) {
        self.interactor = interactor
        self.router = router
        self.interactor.attach(presenter: self)
    }
}

// MARK: - Extension -

// MARK: Presenter Protocol
extension NSSchoolsPresenter: NSSchoolsPresenterProtocol {

    func present() {
        self.router.present(presenter: self)
    }

    func attach(view: NSSchoolsViewProtocol?) {
        self.view = view
    }

    func viewDidLoad() {
        self.interactor.fetchSchools()
    }

    func numberOfRowsInSection(section: Int) -> Int {
        guard !sections.isEmpty else { return 0 }
        return self.items[self.sections[section]]?.count ?? 0
    }

    func cellForRowAt(indexPath: IndexPath) -> NSSchoolsEntity? {
        return self.items[self.sections[indexPath.section]]?[indexPath.row]
    }

    func titleForHeaderInSection(section: Int) -> String {
        return self.sections[section]
    }

    func didSelectRowAt(indexPath: IndexPath) {
        guard let school = self.items[self.sections[indexPath.section]]?[indexPath.row] else {
            return
        }
        router.goToSchoolDetail(parent: self.view, school: school)
    }

    func numberOfSections() -> Int {
        return self.sections.count
    }

    func sectionIndexTitles() -> [String] {
        return self.sections
    }

    func heightForRowAt(widthContainer: CGFloat, indexPath: IndexPath) -> CGFloat {
        // Code to automatically calculate cell heigh
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: widthContainer - staticWidthCell, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = titleNumberOfLines
        label.font = UIFont.systemFont(ofSize: UIFont.labelFontSize, weight: .regular)
        label.text = self.items[self.sections[indexPath.section]]?[indexPath.row].schoolName
        let size = label.sizeThatFits(CGSize(width: label.frame.width, height: CGFloat.greatestFiniteMagnitude))
        let height = size.height + staticHeightCell
        return height
    }
}

// MARK: Interactor Output Protocol
extension NSSchoolsPresenter: NSSchoolsInteractorOutputProtocol {
    func showSchools(schools: [NSSchoolsEntity]) {
        self.items = Dictionary(grouping: schools) { String($0.schoolName.prefix(1)).uppercased() }
        self.sections = items.keys.sorted()
        view?.showSchools()
    }

    func showError(_ message: String) {
        view?.showError(message)
    }
}
