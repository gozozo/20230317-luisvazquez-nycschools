//
//  NSSchoolsProtocols.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

// MARK: View Protocol
protocol NSSchoolsViewProtocol: AnyObject {
    func showSchools()
    func showError(_ message: String)
}

// MARK: Interactor Protocol
protocol NSSchoolsInteractorProtocol {
    func attach(presenter: NSSchoolsInteractorOutputProtocol?)
    func fetchSchools()
}

// MARK: Interactor Output Protocol
protocol NSSchoolsInteractorOutputProtocol: AnyObject {
    func showError(_ message: String)
    func showSchools(schools: [NSSchoolsEntity])
}

// MARK: Presenter protocol
protocol NSSchoolsPresenterProtocol {
    func present()
    func attach(view: NSSchoolsViewProtocol?)
    func viewDidLoad()

    func numberOfRowsInSection(section: Int) -> Int
    func cellForRowAt(indexPath: IndexPath) -> NSSchoolsEntity?
    func titleForHeaderInSection(section: Int) -> String
    func didSelectRowAt(indexPath: IndexPath)
    func numberOfSections() -> Int
    func sectionIndexTitles() -> [String]
    func heightForRowAt(widthContainer: CGFloat, indexPath: IndexPath) -> CGFloat
}

// MARK: Router protocol
protocol NSSchoolsRouterProtocol: AnyObject {
    func present(presenter: NSSchoolsPresenterProtocol)
    func goToSchoolDetail(parent: NSSchoolsViewProtocol?, school: NSSchoolsEntity)
}
