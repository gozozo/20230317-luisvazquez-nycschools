//
//  AppNavigation.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//

import UIKit

class AppNavigator {

    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let viewController = createInitialModule()
        let navigationController = UINavigationController(rootViewController: viewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    private func createInitialModule() -> UIViewController {
        // Aquí se inicializa el primer módulo de la aplicación, basado en la arquitectura VIPER
        // Por ejemplo, si el módulo inicial es el de Login, podrías hacer algo como esto:
        let rootInteractor = NSSchoolsInteractor()
        let rootRoute = NSSchoolsRouter(parentController: nil)
        let rootPresenter = NSSchoolsPresenter(interactor: rootInteractor, router: rootRoute)
        let rootViewController = NSSchoolsViewController(presenter: rootPresenter)
        rootPresenter.attach(view: rootViewController)

        return rootViewController
    }
}
