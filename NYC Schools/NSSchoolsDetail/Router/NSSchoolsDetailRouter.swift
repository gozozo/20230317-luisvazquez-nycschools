//
//  NSSchoolsDetailRouter.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class NSSchoolsDetailRouter {
    private weak var parentController: UIViewController?

    init(parentController: UIViewController?) {
        self.parentController = parentController
    }

}

// MARK: - Extension -
extension NSSchoolsDetailRouter: NSSchoolsDetailRouterProtocol {
    func present(presenter: NSSchoolsDetailPresenterProtocol) {
        let controller = NSSchoolsDetailViewController(presenter: presenter)
        presenter.attach(view: controller)
        if let navigation = parentController?.navigationController {
            navigation.pushViewController(controller, animated: true)
        }
    }
}
