//
//  NSSchoolsDetailModule.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import UIKit

public final class NSSchoolsDetailModule {
    private var presenter: NSSchoolsDetailPresenterProtocol

    public init(baseController: UIViewController?, school: NSSchoolsEntity) {
        let router = NSSchoolsDetailRouter(parentController: baseController)
        let interactor = NSSchoolsDetailInteractor()
        self.presenter = NSSchoolsDetailPresenter(
            interactor: interactor,
            router: router,
            school: school
        )
    }

    public func present() {
        self.presenter.present()
    }
}
