//
//  NSSchoolsDetailViewController.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class NSSchoolsDetailViewController: UIViewController {

    private let metersLatLon: Double = 1500
    private let cornerRadius: CGFloat = 20

    // MARK: Presenter
    private(set) var presenter: NSSchoolsDetailPresenterProtocol

    // MARK: Constructor
    init(presenter: NSSchoolsDetailPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "NSSchoolsDetailViewController", bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: IBActions
    @IBAction func onChangeSection(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            detailView.isHidden = false
            contactView.isHidden = true
        } else {
            detailView.isHidden = true
            contactView.isHidden = false
        }
    }

    @IBAction func openPhone(_ sender: Any) {
        let school = presenter.getSchool()
        let number = String(school.phoneNumber.unicodeScalars.filter { CharacterSet.decimalDigits.contains($0)})
        if let url = URL(string: "tel://\(number)") {
            UIApplication.shared.open(url)
        } else {
            showError("The operation could not be completed.")
        }
    }

    @IBAction func openMail(_ sender: Any) {
        let school = presenter.getSchool()
        if MFMailComposeViewController.canSendMail(), let email = school.schoolEmail {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            present(mail, animated: true)
        } else {
            showError("The operation could not be completed.")
        }
    }

    @IBAction func openWebsite(_ sender: Any) {
        let school = presenter.getSchool()
        if let url = URL(string: "http://\(school.website)") {
            UIApplication.shared.open(url)
        }
    }

    // MARK: IBOUtlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var contactView: UIView!

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.presenter.viewDidLoad()
    }

    // MARK: Private methods
    private func setupView() {
        let backButton = UIBarButtonItem()
        backButton.title = "Back"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        let school = presenter.getSchool()

        titleLabel.text = school.schoolName
        subtitleLabel.text = "\(school.primaryAddressLine1), \(school.city) \(school.stateCode) \(school.zip)"
        detailLabel.text = school.overviewParagraph
        phoneLabel.text = school.phoneNumber
        emailLabel.text = school.schoolEmail
        websiteLabel.text = school.website

        if let latitude = Double(school.latitude ?? ""), let longitude =  Double(school.longitude ?? "") {
            mapView.delegate = self
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.addAnnotation(annotation)
            let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: metersLatLon, longitudinalMeters: metersLatLon)
            mapView.setRegion(region, animated: true)
        }

    }
}

// MARK: - Extensions -
extension NSSchoolsDetailViewController: NSSchoolsDetailViewProtocol {

    func showSatSchool(sat: NSSATSchoolEntity) {
        self.readingLabel.text = sat.satCriticalReadingAvgScore
        self.writingLabel.text = sat.satWritingAvgScore
        self.mathLabel.text = sat.satMathAvgScore
        loadingView.removeFromSuperview()
    }

    func showError(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(actionOk)
        present(alertController, animated: true, completion: nil)
    }
}

extension NSSchoolsDetailViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       let identifier = "pinAnnotation" //The pin view identifier
       var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView

       if annotationView == nil {
           annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
           annotationView?.canShowCallout = true
       } else {
           annotationView?.annotation = annotation
       }
       return annotationView
    }
}

extension NSSchoolsDetailViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
