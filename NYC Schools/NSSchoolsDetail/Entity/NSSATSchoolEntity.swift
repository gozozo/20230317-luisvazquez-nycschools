//
//  NSSchoolsDetailEntity.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

public struct NSSATSchoolEntity: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
       case dbn
       case schoolName = "school_name"
       case numOfSatTestTakers = "num_of_sat_test_takers"
       case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
       case satMathAvgScore = "sat_math_avg_score"
       case satWritingAvgScore = "sat_writing_avg_score"
    }
}
