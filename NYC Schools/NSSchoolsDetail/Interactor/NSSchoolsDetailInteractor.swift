//
//  NSSchoolsDetailInteractor.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation
import Alamofire

final class NSSchoolsDetailInteractor {
    private weak var presenter: NSSchoolsDetailInteractorOutputProtocol?
}

// MARK: - Extension -

extension NSSchoolsDetailInteractor: NSSchoolsDetailInteractorProtocol {

    func attach(presenter: NSSchoolsDetailInteractorOutputProtocol?) {
        self.presenter = presenter
    }

    func fetchSatSchool(dbn: String) {
        AF.request("https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)").validate()
            .responseDecodable(of: [NSSATSchoolEntity].self) { response in
                switch response.result {
                case .success(let data):
                    self.presenter?.showSatSchool(satSchool: data[0])
                case .failure(let error):
                    self.presenter?.showError(error.localizedDescription)
                }
            }
    }
}
