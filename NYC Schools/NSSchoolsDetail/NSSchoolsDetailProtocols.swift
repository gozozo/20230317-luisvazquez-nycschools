//
//  NSSchoolsDetailProtocols.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

// MARK: View Protocol
protocol NSSchoolsDetailViewProtocol: AnyObject {
    func showSatSchool(sat: NSSATSchoolEntity)
    func showError(_ message: String)
}

// MARK: Interactor Protocol
protocol NSSchoolsDetailInteractorProtocol {
    func attach(presenter: NSSchoolsDetailInteractorOutputProtocol?)
    func fetchSatSchool(dbn: String)
}

// MARK: Interactor Output Protocol
protocol NSSchoolsDetailInteractorOutputProtocol: AnyObject {
    func showError(_ message: String)
    func showSatSchool(satSchool: NSSATSchoolEntity)
}

// MARK: Presenter protocol
protocol NSSchoolsDetailPresenterProtocol {
    func present()
    func attach(view: NSSchoolsDetailViewProtocol?)
    func viewDidLoad()
    func getSchool() -> NSSchoolsEntity
}

// MARK: Router protocol
protocol NSSchoolsDetailRouterProtocol: AnyObject {
    func present(presenter: NSSchoolsDetailPresenterProtocol)
}
