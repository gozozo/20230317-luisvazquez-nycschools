//
//  NSSchoolsDetailPresenter.swift
//  NYC Schools
//
//  Created by Luis Enrique Vazquez Escobar on 16/03/23.
//  Copyright (c) 2023 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

final class NSSchoolsDetailPresenter {
    private weak var view: NSSchoolsDetailViewProtocol?
    private var interactor: NSSchoolsDetailInteractorProtocol
    private var router: NSSchoolsDetailRouterProtocol

    private var school: NSSchoolsEntity
    private var satSchool: NSSATSchoolEntity?

    init(
        interactor: NSSchoolsDetailInteractorProtocol,
        router: NSSchoolsDetailRouterProtocol,
        school: NSSchoolsEntity
    ) {
        self.interactor = interactor
        self.router = router
        self.school = school
        self.interactor.attach(presenter: self)
    }
}

// MARK: - Extension -

// MARK: Presenter Protocol
extension NSSchoolsDetailPresenter: NSSchoolsDetailPresenterProtocol {

    func present() {
        router.present(presenter: self)
    }

    func attach(view: NSSchoolsDetailViewProtocol?) {
        self.view = view
    }

    func viewDidLoad() {
        self.interactor.fetchSatSchool(dbn: school.dbn)
    }

    func getSchool() -> NSSchoolsEntity {
        return self.school
    }
}

// MARK: Interactor Output Protocol
extension NSSchoolsDetailPresenter: NSSchoolsDetailInteractorOutputProtocol {

    func showSatSchool(satSchool: NSSATSchoolEntity) {
        view?.showSatSchool(sat: satSchool)
    }

    func showError(_ message: String) {
        view?.showError(message)
    }

}
